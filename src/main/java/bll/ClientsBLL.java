package bll;
import bll.validators.NameValidator;
import bll.validators.Validator;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dao.ClientDAO;
import model.Clients;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**Clasa pentru Bussines Logical Layer,CLIENTS*/
public class ClientsBLL {

    private List<Validator<Clients>> validators;
    int pdfID = 1;

    /**Initializarea listei validators unde se tin validarile pentru clienti*/
    public ClientsBLL() {
        validators = new ArrayList<Validator<Clients>>();
        validators.add(new NameValidator());
    }

    /** Se valideaza insertia in clienti
     * @param c
     * Clientul pe care vrem sa-l introducem
     * @return
     * Returneaza id-ul clientulu inserat*/
    public int insertClient(Clients c) {

        for (Validator<Clients> v : validators) {
            v.validate(c);
        }
        return ClientDAO.insert(c);
    }
    /**Se valideaza stergerea din clienti
     * @param c
     * Clientul pe care vrem sa-l stergem
     * @return
     * Returneaza id-ul clientului sters*/
    public int deleteClient(Clients c){

        for (Validator<Clients> v : validators) {
            v.validate(c);
        }
        return ClientDAO.delete(c);
    }

    /**Metoda in care se genereaza Report client
     * @throws FileNotFoundException
     * Daca fisierul nu este gasit
     * @throws DocumentException
     * Daca documentul nu exista*/
    public void generateReport() throws FileNotFoundException, DocumentException {

        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("Clients Report "+ (pdfID++) +".pdf"));

        document.open();

        PdfPTable table = new PdfPTable(2);
        addTableHeader(table);
        addRows(table);

        document.add(table);
        document.close();

    }

    /**Adaugarea headere-lor pentru Report client
     * @param table
     * Tabelul in care vrem sa introducem*/
    public void addTableHeader(PdfPTable table){

        Stream.of("name", "address")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    /**Adaugarea de clienti in tabel
     * @param table
     * Tabelul in care vrem sa introducem*/
    public void addRows(PdfPTable table){

        for (Clients c: ClientDAO.clients) {
            table.addCell(c.getName());
            table.addCell(c.getAddress());
        }
    }


}
