package bll;

import bll.validators.NameValidator;
import bll.validators.PriceValidator;
import bll.validators.QuantityValidator;
import bll.validators.Validator;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dao.ClientDAO;
import dao.ProductDAO;
import model.Clients;
import model.Products;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
/**Clasa folosita pentru Bussines Logical Layer, PRODUCTS*/
public class ProductsBLL {

    private List<Validator<Products>> validators;
    int pdfID = 1;
    /**Initialiazarea listei validators in care tinem toti validatorii din products*/
    public ProductsBLL() {
        validators = new ArrayList<Validator<Products>>();
        validators.add(new PriceValidator());
        validators.add(new QuantityValidator());
    }

    /**Validarea insertiei in products
     * @param p
     * Produsul pe care dorim sa-l inseram
     * @return
     * Returneaza ID-ul produsului inserat*/
    public int insertProduct(Products p) {

        for (Validator <Products> v : validators) {
            v.validate(p);
        }
        return ProductDAO.insert(p);
    }
    /**Validarea update-ului in products*
     * @param p
     * Produsul pe care dorim sa-l updatam
     * @param updateValue
     * Valoarea cu care dorim sa updatam cantitatea produsului
     * @return
     * Returneaza ID-ul produsului updatat*/

    public int updateProduct(Products p,int updateValue){

        for(Validator<Products> v : validators){
            v.validate(p);
        }

        return ProductDAO.update(p,updateValue);
    }

    /**Validarea stergerii in products
     * @param p
     * Produsul pe care dorim sa-l stergem
     * @return
     * Returneaza ID-ul produsului sters*/
    public int deleteProduct(Products p){

        for(Validator<Products> v : validators){
            v.validate(p);
        }

        return ProductDAO.delete(p);
    }
    /**Metoda pentru generarea Report product
     * @throws FileNotFoundException
     * Daca fisierul nu este gasit
     * @throws DocumentException
     * Daca documentul nu exista*/
    public void generateReport() throws FileNotFoundException, DocumentException {

        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("Product Report "+ (pdfID++) +".pdf"));

        document.open();

        PdfPTable table = new PdfPTable(3);
        addTableHeader(table);
        addRows(table);

        document.add(table);
        document.close();

    }
    /**Metoda pentru adaugarea headere-lor in tabelul Report product
     * @param table
     * Tabelul in care dorim sa inseram*/
    public void addTableHeader(PdfPTable table){

        Stream.of("name", "quantity","price")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }
    /**Metoda pentru adaugarea elementelor in tabelul Report product
     * @param table
     * Tabelul in care dorim sa inseram*/
    public void addRows(PdfPTable table){

        for (Products p: ProductDAO.products) {
            table.addCell(p.getName());
            table.addCell(String.valueOf(p.getQuantity()));
            table.addCell(String.valueOf(p.getPrice()));
        }
    }

}
