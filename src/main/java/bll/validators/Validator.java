package bll.validators;
/**Interfata folosita pentru a valida Clients,Orders si Products*/
public interface Validator<T> {

    public void validate(T t);
}