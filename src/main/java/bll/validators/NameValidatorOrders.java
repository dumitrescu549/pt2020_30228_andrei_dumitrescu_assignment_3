package bll.validators;
import model.Orders;

/** Clasa pentru validarea comenzilor*/
public class NameValidatorOrders implements Validator<Orders> {

    /** In cazul in care numele clientului nu este introdus corect(un singur nume),primi o exceptie IllegalArgumentException
     * @throws IllegalArgumentException
     * Daca numele este introdus gresit
     * @param o
     * Comanda pe care dorim s-o validam*/
    public void validate(Orders o){

        String[] val = o.getName().split(" ");
        if(val.length == 1)
            throw new IllegalArgumentException("Introduce the full name !");
    }
}