package bll.validators;
import bll.validators.Validator;
import model.Orders;

/** Clasa pentru validarea comenzilor*/
public class PriceValidatorOrders implements Validator<Orders> {

    /**In cazul in care pretul din comenzi nu este macar 0, primim o exceptie IllegalArgumentException
     * @throws IllegalArgumentException
     * Daca pretul nu este peste 0
     * @param o
     * Order-ul pe care dorim sa-l validam*/
    public void validate(Orders o){
        if(o.getPrice()<=0){
            throw new IllegalArgumentException("Price must be above 0!");
        }
    }
}
