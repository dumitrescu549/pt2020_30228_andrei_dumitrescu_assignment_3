package bll.validators;

import model.Clients;
/** Clasa pentru validarea numelor clientilor**/
public class NameValidator implements Validator<Clients> {
    /** In cazul in care numele nu este introdus corect(este introdus un singur nume), primim un IllegalArgumentException
     * @throws IllegalArgumentException
     * Daca numele este introdus gresit
     * @param c
     * Clientul pe care dorim sa-l validam*/
    public void validate(Clients c){

        String[] val = c.getName().split(" ");
        if(val.length == 1)
            throw new IllegalArgumentException("Introduce the full name !");
    }
}
