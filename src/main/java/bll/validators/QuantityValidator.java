package bll.validators;

import model.Products;

/**Clasa pentru validarea produselor*/
public class QuantityValidator implements Validator<Products> {
    /**In cazul in care cantitatea introdusa nu este macar 0, primim o exceptie IllegalArgumentException
     * @throws IllegalArgumentException
     * Daca cantitatea nu este peste 0
     * @param p
     * Produsul pe care dorim sa-l validam*/
    public void validate(Products p){
        if(p.getQuantity()<0){
            throw new IllegalArgumentException("Quantity value must be above 0!");
        }
    }

}
