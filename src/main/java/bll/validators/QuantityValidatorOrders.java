package bll.validators;
import model.Orders;

/**Clasa pentru validarea comenzilor*/
public class QuantityValidatorOrders implements Validator<Orders> {
    /**In cazul in care cantitatea este sub 0, primi o exceptie IllegalArgumentException
     * @throws IllegalArgumentException
     * Daca cantitatea nu este peste 0
     * @param o
     * Orderul pe care dorim sa-l validam*/
    public void validate(Orders o){
        if(o.getQuantity()<=0){
            throw new IllegalArgumentException("Quantity value must be above 0!");
        }
    }

}