package bll.validators;

import model.Products;
/** Clasa pentru validarea produselor*/
public class PriceValidator implements Validator<Products> {

    /** In cazul in care pretul este mai mic decat 0(trebuie sa fie macar 0), primim o exceptie IllegalArgumentException
     * @throws IllegalArgumentException
     *Daca pretul nu este peste 0
     * @param p
     *Produsul pe care dorim sa-l validam*/
    public void validate(Products p){
        if(p.getPrice()<0){
            throw new IllegalArgumentException("Price must be above 0!");
        }
    }
}
