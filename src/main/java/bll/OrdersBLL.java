package bll;

import bll.validators.*;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Orders;
import model.Products;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
/**Clasa folosita pentru Bussines Logic Layer, ORDERS*/

public class OrdersBLL {

    private List<Validator<Orders>> validators;
    int pdfID = 1;
    int billID = 1;
    int failedBillId = 1;
    /**Initializarea listei validators in care se tin toti validatorii pentru orders*/
    public OrdersBLL() {
        validators = new ArrayList<Validator<Orders>>();
        validators.add(new PriceValidatorOrders());
        validators.add(new QuantityValidatorOrders());
    }

    /**Se valideaza insertia in comenzi
     * @param o
     * Comanda pe care vrem s-o inseram
     * @return
     * Retruneaza id-ul comenzii inserate*/
    public int insertOrder(Orders o){

        for (Validator <Orders> v : validators) {
            v.validate(o);
        }
        return OrderDAO.insert(o);
    }

    /**Metoda pentru generarea bill-ului
     * @param orderID
     * ID-ul clientului pentru care generam bill-ul
     * @throws FileNotFoundException
     * Daca fisierul nu s-a gasit
     * @throws DocumentException
     * Daca documentul nu s-a gasit*/
    public void generateBill(int orderID) throws FileNotFoundException, DocumentException {

        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("Bill "+ (billID++) +".pdf"));

        document.open();

        PdfPTable table = new PdfPTable(4);
        addTableHeader(table);
        addRows(table,orderID);

        document.add(table);
        document.close();

    }
    /**Metdoa pentru adaugarea headere-lor pentru Bill
     * @param table
     * Tabelul in care vrem sa introducem*/
    public void addTableHeader(PdfPTable table){

        Stream.of("clientName","product", "quantity","price")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }
    /**Metoda pentru adaugarea elementelor in tabelul Bill
     * @param table
     * Tabelul in care vrem sa introducem
     * @param orderID
     * ID-ul clientului pe care vrem sa-l introducem in tabel*/
    public void addRows(PdfPTable table,int orderID) {

        for (Orders o : OrderDAO.orders) {
            if(o.getID() == orderID){
                table.addCell(o.getName());
                table.addCell(o.getProduct());
                table.addCell(String.valueOf(o.getQuantity()));
                table.addCell(String.valueOf(o.getPrice()));
            }
        }
    }
    /**Metoda pentru generarea mesajului de eroare
     * @throws FileNotFoundException
     * Daca fisierul nu a fost gasit
     * @throws DocumentException
     * Daca documentul nu exista*/
    public void generateFailedBill() throws FileNotFoundException, DocumentException {

        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("Failed Bill "+(failedBillId++)+".pdf"));

        document.open();
        Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
        Chunk chunk = new Chunk( "The demand is too high ! Our stock is too low.", font);

        document.add(chunk);
        document.close();
    }
    /**Metoda pentru generarea Report orders
     *  @throws FileNotFoundException
     *  Daca fisierul nu a fost gasit
     *  @throws DocumentException
     *  Daca documentul nu exista*/
    public void generateReport() throws FileNotFoundException, DocumentException {

        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("Orders report " + (pdfID++) +".pdf"));

        document.open();

        PdfPTable table = new PdfPTable(5);
        addTableHeaderReport(table);
        addRowsReport(table);

        document.add(table);
        document.close();

    }
    /**Metoda pentru a introduce headere in tabelul Report order
     * @param table
     * Tabelul in care vrem sa intorducem*/
    public void addTableHeaderReport(PdfPTable table){

        Stream.of("clientName","product", "quantity","price","Order ID")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }
    /**Metoda pentru adaugarea in tabelul Report order*
     * @param table
     * Tabelul in care vrem sa introducem*/
    public void addRowsReport(PdfPTable table) {

        for (Orders o : OrderDAO.orders) {

            table.addCell(o.getName());
            table.addCell(o.getProduct());
            table.addCell(String.valueOf(o.getQuantity()));
            table.addCell(String.valueOf(o.getPrice()));
            table.addCell(String.valueOf(o.getID()));

        }
    }

}
