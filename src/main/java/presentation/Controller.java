package presentation;

import java.io.File;
import java.io.FileNotFoundException;

/**Clasa pentru comunicarea dintre model si interfata(care e reprezentata de un fisier de input si PDF-urile de output*/
public class Controller {

    private File file;
    private Model model;

    /**@param file
     *Fisierul de input
     * @param model
     *Initializarea clasei model cu variabilele sale
     * @throws FileNotFoundException
     * Daca fisierul nu exista sau nu s-a putut deschide*/
    public Controller(File file,Model model) throws FileNotFoundException {
        this.file = file;
        this.model = model;
        model.parseFile(file);
    }


}
