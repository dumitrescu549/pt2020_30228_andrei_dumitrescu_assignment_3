package presentation;

import bll.ClientsBLL;
import dao.ClientDAO;
import model.Clients;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**Clasa pentru manipularea input-ului*/
public class Model {

    public String[] commands = new String[1000];/*String-uri in care se tin comenzile initiale*/
    public String[] commandName = new String[1000];/*String-uri in care se vor tine numele comenzilor*/
    public String[] commandArgs = new String[1000];/*String-uri in care se vor tine argumentele comenzilor*/
    public int index = 0;/*Variabila pentru parcurgerea comenzilor din fisier*/

    /**Metoda pentru parsarea fiserului de input
     * @param file
     * Fisierul de input pe care dorim sa-l parsam
     * @throws FileNotFoundException
     * Daca fiserul nu s-a putut deschide*/
    public void parseFile(File file) throws FileNotFoundException {

        Scanner scan = new Scanner(file);

        while(scan.hasNext()){

            commands[index] = scan.nextLine();//
            String[] split = commands[index].split(": ");
            if(split.length == 1){
                commandName[index] = split[0];
                commandArgs[index] = "Empty";
            }
            else {
                commandName[index] = split[0];
                commandArgs[index] = split[1];
            }
            index++;
        }
   }

}
