package dao;

import connection.ConnectionFactory;
import model.Clients;
import model.Products;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**Clasa folosita pentru query-urile de la produse*/
public class ProductDAO {

    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO products (name,quantity,price)"
            + " VALUES (?,?,?)";/**Query insert*/
    private static final String updateStatementString = "UPDATE products SET quantity = ? WHERE name = ?";/*Query update*/
    private static final String deleteStatementString = "DELETE FROM products WHERE name= ?";/*Query delete*/
    public static List<Products> products = new ArrayList<Products>();/*Lista in care tinem produsele pentru generarea Report product*/

    /**Metoda pentru introducerea produselor in baza de date
     * @param p
     * Produsul pe care dorim sa-l inseram
     * @return
     * ID-ul produsulu inserat*/
    public static int insert(Products p){

        int insertedId = -1;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;

        try{
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, p.getName());
            insertStatement.setInt(2, p.getQuantity());
            insertStatement.setFloat(3,p.getPrice());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if(rs.next()){
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductsDAO:insert " + e.getMessage());
        }

        finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }

        products.add(p);/*Updatarea listei "products" */
        return insertedId;
    }

    /**Metoda pentru updatarea produselor in baza de date
     * @param p
     * Produsul pe care dorim sa-l updatam
     * @param updateValue
     * Valoarea cu care vrem sa updatam cantitatea produsului
     * @return
     * ID-ul produsului updatat*/
    public static int update(Products p,int updateValue){

        int insertedId = -1;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement updateStatement = null;

        try{
            updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
            updateStatement.setInt(1, p.getQuantity() + updateValue);
            updateStatement.setString(2,p.getName());
            updateStatement.executeUpdate();

            ResultSet rs = updateStatement.getGeneratedKeys();
            if(rs.next()){
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductsDAO:insert " + e.getMessage());
        }

        finally {
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }

        p.setQuantity(p.getQuantity() + updateValue);
        Products aux = p;

        for(Products prod : products){
            if(prod.getName().equals(p.getName())){
                aux = prod;
            }
        }

        products.remove(aux);/*Updatarea listei "products"*/
        products.add(p);

        return insertedId;
    }

    /**Metoda pentru stergerea produselor din baza de date
     * @param p
     * Produsul pe care dorim sa-l stergem
     * @return
     * ID-ul produsului sters*/
    public static int delete(Products p){

        int deletedId = -1;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;

        try{
            deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
            deleteStatement.setString(1, p.getName());
            deleteStatement.executeUpdate();

            ResultSet rs = deleteStatement.getGeneratedKeys();
            if(rs.next()){
                deletedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
        }

        for(Products prod : products){
            if(prod.getName().equals(p.getName())){
                p = prod ;
            }
        }
        products.remove(p);/*Updatarea listei "products"*/
        return deletedId;
    }

}
