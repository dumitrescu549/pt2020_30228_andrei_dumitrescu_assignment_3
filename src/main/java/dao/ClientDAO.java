package dao;
import connection.ConnectionFactory;
import model.Clients;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**Clasa folosita pentru query-urile de la clienti*/
public class ClientDAO {

    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO clients (name,address)"
            + " VALUES (?,?)";/*Query insert*/
    private static final String deleteStatementString = "DELETE FROM clients WHERE name= ? AND address= ?";/*Query delete*/
    public static List<Clients> clients = new ArrayList<Clients>();/*Lista folosita pentru tinerea clientilor, folosita pentru generarea raportului*/

    /**Metdoa folosita pentru a introduce un client in baza de date
     * @param c
     * Clientul pe care dorim sa-l inseram
     * @return
     * Returneaza ID-ul clientului inserat*/
    public static int insert(Clients c){

        int insertedId = -1;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;

        try{
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, c.getName());
            insertStatement.setString(2, c.getAddress());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if(rs.next()){
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientsDAO:insert " + e.getMessage());
        }

        finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }

        clients.add(c);/*Updatarea listei "clients"*/
        return insertedId;
    }

    /**Metoda pentru a sterge un client din baza de date*
     * @param c
     * Clientul pe care dorim sa-l stergem
     * @return
     * Returneaza ID-ul clientului sters*/
    public static int delete(Clients c){

        int deletedId = -1;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;

        try{
            deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
            deleteStatement.setString(1, c.getName());
            deleteStatement.setString(2,c.getAddress());
            deleteStatement.executeUpdate();

            ResultSet rs = deleteStatement.getGeneratedKeys();
            if(rs.next()){
                deletedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientsDAO:delete " + e.getMessage());
        }

        for(Clients cl : clients){
            if(c.getName().equals(cl.getName())){
                c = cl;
            }
        }
        clients.remove(c);/*Updatarea listei "clients"*/
        return deletedId;
    }
}
