package dao;

import connection.ConnectionFactory;
import model.Orders;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**Clasa folosita pentru query-urile de la comenzi*/
public class OrderDAO {
    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO orders (clientName,product,quantity,price,ID)"
            + " VALUES (?,?,?,?,?)";/*Query insert*/
    public static List<Orders> orders = new ArrayList<Orders>();/*Lista in care tinem comenzile pentru generarea Raport orders*/

    /**Metoda pentru inserarea comenzilor in baza de date
     * @param o
     * Comanda pe care dorim s-o inseram
     * @return
     * ID-ul comenzii care a fost inserata*/
    public static int insert(Orders o){

        int insertedId = -1;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;

        try{
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, o.getName());
            insertStatement.setString(2,o.getProduct());
            insertStatement.setInt(3,o.getQuantity());
            insertStatement.setFloat(4,o.getPrice());
            insertStatement.setInt(5,o.getID());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if(rs.next()){
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
        }

        finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }

        orders.add(o);/**Updatarea listei "orders"*/
        return insertedId;
    }
}
