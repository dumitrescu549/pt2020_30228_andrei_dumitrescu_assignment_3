package model;
/**Clasa in care se tin datele despre comenzi*/
public class Orders {

    String clientName;
    String product;
    int quantity;
    float price;
    int ID;

    /**@param clientName
     * Numele clientului
     * @param product
     * Produsulu cumparat
     * @param quantity
     * Cantitatea produsului
     * @param price
     * Pretul total
     * @param ID
     * ID-ul comenzii*/
    public Orders(String clientName,String product,int quantity,float price,int ID){
        this.clientName = clientName;
        this.product = product;
        this.quantity = quantity;
        this.price = price;
        this.ID = ID;
    }
    /**@return
     * Retruneaza numele clientului*/
    public String getName(){
        return clientName;
    }
    /**@return
     * Returneaza numele produsului*/
    public String getProduct(){
        return product;
    }
    /**@return
     * Returneaza cantitatea produsului cumparat*/
    public int getQuantity(){
        return quantity;
    }
    /**@return
     * Returneaza pretul total*/
    public float getPrice(){
        return price;
    }
    /**@return
     * Returneaza ID-ul comenzii*/
    public int getID(){return ID;}
}
