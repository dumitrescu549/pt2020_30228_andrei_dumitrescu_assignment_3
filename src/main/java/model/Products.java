package model;

import com.itextpdf.text.Image;

public class Products { /**Clasa in care se tin datele depsre produse*/
    String name;
    int quantity;
    float price;

    /**@param name
     * Numele produsului
     * @param quantity
     * Cantitatea produsului
     * @param price
     * Pretul produsului/bucata*/
    public Products(String name, int quantity, float price){
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    /**@return
     * Returneaza numele produsului*/
    public String getName(){
        return name;
    }
    /**@return
     * Returneaza cantitatea produsului*/
    public int getQuantity(){
        return quantity;
    }

    /**@return
     * Returneaza pretul produsului / bucata*/
    public float getPrice(){return price; }

    /**@param value
     * Seteaza valoarea produsului la valoarea lui "value"*/
    public void setQuantity(int value){
        this.quantity = value;
    }


}
