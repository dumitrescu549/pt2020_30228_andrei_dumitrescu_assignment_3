package model;
/**Clasa in care se tin datele despre clienti*/
public class Clients {
    String name;
    String address;

    /**Initializarea clientilor
     * @param name
     * Numele clientului
     * @param address
     * Adresa clientului*/
    public Clients(String name, String address){
        this.name = name;
        this.address = address;
    }

    /** @return
     * Returneaza numele clientului*/
    public String getName(){
        return name;
    }

    /**@return
     *Returneaza adresa clientului*/
    public String getAddress(){
        return address;
    }
}
