package start;
import bll.ClientsBLL;
import bll.OrdersBLL;
import bll.ProductsBLL;
import com.itextpdf.text.DocumentException;
//import com.mysql.cj.xdevapi.Clients;
import dao.ClientDAO;
import dao.ProductDAO;
import model.Clients;
import model.Orders;
import model.Products;
import presentation.Controller;
import presentation.Model;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**Clasa pentru pornirea aplicatiei*/
public class Start {

    /**Metoda main
     * @param argv
     * Argumentele introduse de la tastatura
     * @throws FileNotFoundException
     * In cazul in care nu s-a gasit fisierul
     * @throws DocumentException
     * In cazul in care nu s-a gasit documentul*/
    public static void main(String argv[]) throws FileNotFoundException, DocumentException {

        Model model = new Model();
        File file = new File(argv[0]);
        Controller controller = new Controller(file,model);
        ClientsBLL cb = new ClientsBLL();
        ProductsBLL pb = new ProductsBLL();
        OrdersBLL ob = new OrdersBLL();
        int ID = 1;

        for(int i = 0 ; i < model.index ; i++) { /*Loop in care parcurgem fisierul de input*/

            if (model.commandName[i].equals("Insert client")) {/*Pornirea insertiei clientilor in cazul in care comanda cere asta*/
                String[] split = model.commandArgs[i].split(", ");
                if(split.length == 1){
                    throw new ArrayIndexOutOfBoundsException("Introduce all the fields !");
                }
                else{
                    Clients c = new Clients(split[0], split[1]);
                    cb.insertClient(c);
                }

            }

            if(model.commandName[i].equals("Delete client")){ /*Pornirea stergerii clientilor in cazul in care comanda cere asta*/

                String[] split = model.commandArgs[i].split(", ");
                if(split.length == 1){
                    throw new ArrayIndexOutOfBoundsException("Introduce all the fields!");
                }
                else{
                    Clients c = new Clients(split[0],split[1]);
                    cb.deleteClient(c);
                }
            }

            if(model.commandName[i].equals("Report client")){/*Pornirea raportului clientilor in cazul in care comanda cere asta*/
                cb.generateReport();
            }

            if(model.commandName[i].equals("Insert product")){/*Pornirea insertiei produselor in cazul in care comanda cere asta*/
                String[] split = model.commandArgs[i].split(", ");
                if(split.length <3){
                    throw new ArrayIndexOutOfBoundsException("Introduce all the fields!");
                }
                else {
                    Products p = new Products(split[0],Integer.parseInt(split[1]),Float.parseFloat(split[2]));
                    int found = 0;
                    for(Products prod : ProductDAO.products){
                        if(prod.getName().equals(p.getName())){
                            found = 1;
                        }
                    }
                    if(found == 0)
                        pb.insertProduct(p);
                    else
                        pb.updateProduct(p,p.getQuantity());
                }
            }


            if(model.commandName[i].equals("Delete Product")){/*Pornirea stergerii produselor in cazul in care comanda cere asta*/
                Products p = new Products("aux",0,0);
                //System.out.println(model.commandArgs[i]);
                for(Products prod : ProductDAO.products){
                    //System.out.println(prod.getName());
                    if(prod.getName().equals(model.commandArgs[i])){
                        p = prod;
                        //System.out.println("A");
                    }
                }
                pb.deleteProduct(p);
            }

            if(model.commandName[i].equals("Report product")){/*Pornirea raportului produselor in cazul in care comanda cere asta*/
                pb.generateReport();
            }

            if(model.commandName[i].equals("Order")){/*Pornirea adaugarii comenzilor in cazul in care comanda cere asta*/

                String[] split = model.commandArgs[i].split(", ");
                if(split.length < 3){
                    throw new ArrayIndexOutOfBoundsException("Introduce all the fields!");
                }
                else{
                    Products aux = new Products("aux",0,0);
                    int quantity = Integer.parseInt(split[2]);
                    String product = split[1];
                    String client = split[0];

                    for(Products p : ProductDAO.products){

                        if( p.getName().equals(product)){ // verificam daca avem cantitatea necesara
                            aux = p;
                        }
                    }

                    if(aux.getQuantity() >= quantity){
                        pb.updateProduct(aux,(-1) * quantity );//updatam lista de produse
                        Orders o = new Orders(client,product,quantity,aux.getPrice() * quantity,ID);
                        ob.insertOrder(o); // creeam comanda
                        ob.generateBill(ID++);
                    }

                    else{
                        ob.generateFailedBill();
                    }
                }
            }

            if(model.commandName[i].equals("Report order")){/*Pornirea raportului comenzilor in cazul in care comanda cere asta*/
                ob.generateReport();
            }

        }
    }
}
